const theme = require('tailwindcss/defaultTheme')

export default {
  'primary': '#74cbc8',
  'secondary': theme.colors.black[800],
  'accent': theme.colors.gray[600],
  'error': theme.colors.red[600],
  'info': theme.colors.blue[600],
  'success': theme.colors.green[500],
  'warning': theme.colors.orange[500],
  'background': '#f2f3f3'
}
